package com.dictionary.urban.network

import com.dictionary.urban.model.SearchList
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiService {

    @GET("/define")
    fun getJsonResponse(@Query("term", encoded = true) term: String): Single<SearchList>
}