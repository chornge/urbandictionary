package com.dictionary.urban.model

import com.google.gson.annotations.SerializedName

data class SearchWord(
    @SerializedName("definition") val definition: String,
    @SerializedName("permalink") val permalink: String,
    @SerializedName("thumbs_up") val thumbs_up: Long,
    @SerializedName("sound_urls") val sound_urls: List<String?> = listOf(),
    @SerializedName("author") val author: String,
    @SerializedName("word") val word: String,
    @SerializedName("defid") val id: Long,
    @SerializedName("current_vote") val current_vote: String,
    @SerializedName("written_on") val written_on: String,
    @SerializedName("example") val example: String,
    @SerializedName("thumbs_down") val thumbs_down: Long
)