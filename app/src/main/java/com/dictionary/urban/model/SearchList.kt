package com.dictionary.urban.model

import com.google.gson.annotations.SerializedName

data class SearchList(
    @SerializedName("list") val list: List<SearchWord>
)
