package com.dictionary.urban.repository

import com.dictionary.urban.model.SearchList
import com.dictionary.urban.network.ApiService
import io.reactivex.Single
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class Repository @Inject constructor(private val apiService: ApiService) {
    fun getWords(term: String): Single<SearchList> = apiService.getJsonResponse(term)
}