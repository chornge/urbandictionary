package com.dictionary.urban.injection

import com.dictionary.urban.repository.Repository
import com.dictionary.urban.util.SchedulerProvider
import com.dictionary.urban.viewmodel.MainActivityViewModel
import dagger.Module
import dagger.Provides

@Module
class MainActivityModule {

    @Provides
    fun provideViewModel(
        repository: Repository,
        schedulerProvider: SchedulerProvider
    ) = MainActivityViewModel(repository, schedulerProvider)
}