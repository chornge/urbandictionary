package com.dictionary.urban.view

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.Gravity
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.SearchView
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.dictionary.urban.R
import com.dictionary.urban.adapter.SearchWordAdapter
import com.dictionary.urban.model.SearchList
import com.dictionary.urban.model.SearchWord
import com.dictionary.urban.viewmodel.MainActivityViewModel
import dagger.android.DaggerActivity
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.subscribeBy
import kotlinx.android.synthetic.main.main_activity.*
import javax.inject.Inject


class MainActivity : DaggerActivity(), SearchView.OnQueryTextListener {

    private val tag = "MainActivity_TAG"
    private val compositeDisposable by lazy { CompositeDisposable() }

    @Inject
    lateinit var mainActivityViewModel: MainActivityViewModel

    private var listToSort = listOf<SearchWord>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity)
        initSearchBar()
        search_view.setOnQueryTextListener(this)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.list_options_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return when (item?.itemId) {
            R.id.filter_by_thumbs_up -> {
                sortThumbsUpAscOrder()
                true
            }

            R.id.filter_by_thumbs_down -> {
                sortThumbsDownAscOrder()
                true
            }

            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun initSearchBar() {
        search_view.setOnSearchClickListener {
            tap_icon_to_begin_text.visibility = View.GONE
        }

        search_view.setOnCloseListener {
            tap_icon_to_begin_text.visibility = View.VISIBLE
            tap_icon_to_begin_text.text = getString(R.string.tap_the_search_icon_to_begin)
            clearRecyclerView()
            false
        }
    }

    private fun hideKeyboard() {
        val imm =
            getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager

        if (imm.isAcceptingText) { // verify if the soft keyboard is open
            imm.hideSoftInputFromWindow(currentFocus.windowToken, 0)
        }
    }

    override fun onQueryTextChange(p0: String?): Boolean {
        return false
    }

    override fun onQueryTextSubmit(text: String?): Boolean {
        progress_bar.visibility = View.VISIBLE
        fetchSearchData(text)
        return true
    }

    private fun sortThumbsUpAscOrder() {
        list_recycler_view.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = SearchWordAdapter(listToSort.sortedWith(compareBy { it.thumbs_up }))
        }
    }

    private fun sortThumbsDownAscOrder() {
        list_recycler_view.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = SearchWordAdapter(listToSort.sortedWith(compareBy { it.thumbs_down }))
        }
    }

    // TODO: refactor to presenter/view-model
    private fun fetchSearchData(text: String?) {
        hideKeyboard()
        compositeDisposable.add(
            mainActivityViewModel.showDataFromApi(text.toString())
                .subscribeBy(onSuccess = {
                    tap_icon_to_begin_text.visibility = View.VISIBLE
                    if (it.list.isNotEmpty()) {
                        listToSort = it.list
                        tap_icon_to_begin_text.visibility = View.GONE
                        populateRecyclerView(it)
                    } else {
                        tap_icon_to_begin_text.visibility = View.GONE
                        val toast = Toast.makeText(
                            applicationContext,
                            R.string.error_word_not_found,
                            Toast.LENGTH_LONG
                        )
                        toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0)
                        toast.show()
                    }
                }, onError = {
                    Log.d(tag, it.message)
                    val toast = Toast.makeText(
                        applicationContext,
                        R.string.error_occurred,
                        Toast.LENGTH_LONG
                    )
                    toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0)
                    toast.show()
                })
        )
        progress_bar.visibility = View.GONE
    }

    private fun populateRecyclerView(it: SearchList) {
        list_recycler_view.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = SearchWordAdapter(it.list)
        }
    }

    private fun clearRecyclerView() {
        list_recycler_view.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = SearchWordAdapter(listOf())
            listToSort = listOf()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        if (!compositeDisposable.isDisposed) {
            compositeDisposable.clear()
            compositeDisposable.dispose()
        }
    }
}
