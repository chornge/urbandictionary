package com.dictionary.urban.view

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.dictionary.urban.R

class SplashActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.splash_activity)

        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
        finish()
    }
}
