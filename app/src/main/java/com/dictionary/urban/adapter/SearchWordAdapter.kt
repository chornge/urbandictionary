package com.dictionary.urban.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.dictionary.urban.model.SearchWord

class SearchWordAdapter(private val list: List<SearchWord>) : RecyclerView.Adapter<SearchWordViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SearchWordViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return SearchWordViewHolder(inflater, parent)
    }

    override fun onBindViewHolder(holder: SearchWordViewHolder, position: Int) {
        val searchWord: SearchWord = list[position]
        holder.bind(searchWord)
    }

    override fun getItemCount(): Int = list.size
}