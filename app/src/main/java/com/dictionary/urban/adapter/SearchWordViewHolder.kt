package com.dictionary.urban.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.dictionary.urban.R
import com.dictionary.urban.model.SearchWord

class SearchWordViewHolder(inflater: LayoutInflater, parent: ViewGroup) :
    RecyclerView.ViewHolder(inflater.inflate(R.layout.list_item, parent, false)) {
    private var mListWord: TextView? = itemView.findViewById(R.id.list_word)
    private var mListDefinition: TextView? = itemView.findViewById(R.id.list_definition)
    private var mListThumbsUp: TextView? = itemView.findViewById(R.id.list_thumbs_up_total)
    private var mListThumbsDown: TextView? = itemView.findViewById(R.id.list_thumbs_down_total)

    fun bind(searchWord: SearchWord) {
        mListWord?.text = searchWord.word
        mListDefinition?.text = searchWord.definition
        mListThumbsUp?.text = searchWord.thumbs_up.toString()
        mListThumbsDown?.text = searchWord.thumbs_down.toString()
    }
}