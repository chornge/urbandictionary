package com.dictionary.urban.viewmodel

import com.dictionary.urban.model.SearchList
import com.dictionary.urban.repository.Repository
import com.dictionary.urban.util.SchedulerProvider
import io.reactivex.Single

class MainActivityViewModel(
    private val repository: Repository,
    private val schedulerProvider: SchedulerProvider
) {
    fun showDataFromApi(term: String): Single<SearchList> = repository.getWords(term)
        .compose(schedulerProvider.getSchedulersForSingle<SearchList>())
}