package com.dictionary.urban

import android.content.Intent
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.rule.ActivityTestRule
import androidx.test.runner.AndroidJUnit4
import com.dictionary.urban.view.MainActivity
import org.hamcrest.Matchers.not
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith


@RunWith(AndroidJUnit4::class)
class MainActivityInstrumentedTest {

    @Rule
    @JvmField
    val activityRule = ActivityTestRule(MainActivity::class.java)

    @Before
    fun intent() {
        val intent = Intent()
        activityRule.launchActivity(intent)
    }

    @Test
    fun progress_bar_is_not_initially_visible() {
        onView(withId(R.id.progress_bar)).check(matches(not(isDisplayed())))
    }

    @Test
    fun text_is_no_longer_visible_when_search_bar_is_clicked() {
        onView(withId(R.id.search_view)).perform(click())
        onView(withId(R.id.tap_icon_to_begin_text)).check(matches(withEffectiveVisibility(Visibility.VISIBLE)))
    }
}
