package com.dictionary.urban

import com.dictionary.urban.model.SearchList
import com.dictionary.urban.model.SearchWord
import com.dictionary.urban.repository.Repository
import com.dictionary.urban.util.SchedulerProvider
import com.dictionary.urban.viewmodel.MainActivityViewModel
import io.reactivex.Single
import io.reactivex.observers.TestObserver
import io.reactivex.schedulers.Schedulers
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations

class MainActivityViewModelTest {

    @Mock
    private lateinit var mockRepository: Repository

    private val schedulerProvider =
        SchedulerProvider(Schedulers.trampoline(), Schedulers.trampoline())

    private lateinit var mainActivityViewModel: MainActivityViewModel

    private val searchResult = SearchWord(
        "It's [literally] [the letter A]. [Why did you look this up]?",
        "http://a.urbanup.com/12901851", 788, listOf(""), "xxTrashyMilkCartonxx",
        "A", 12901851, "", "2018-06-03T00:00:00.000Z",
        "Wow, I really don't understand why you [looked] up THE [FREAKING] [LETTER A]",
        59
    )

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        mainActivityViewModel = MainActivityViewModel(mockRepository, schedulerProvider)

        Mockito.`when`(mockRepository.getWords("a"))
            .thenReturn(Single.just(SearchList(listOf(searchResult))))
    }

    @Test
    fun `get the search id letter a when searching for letter a`() {
        val testObserver = TestObserver<SearchList>()
        mainActivityViewModel.showDataFromApi("a").subscribe(testObserver)
        testObserver.assertNoErrors()

        // testObserver.awaitTerminalEvent()
        testObserver.assertValue { searchList ->
            searchList.list.first().id == searchResult.id
        }
    }

    @Test
    fun `get the search word letter a when searching for letter a`() {
        val testObserver = TestObserver<SearchList>()
        mainActivityViewModel.showDataFromApi("a").subscribe(testObserver)
        testObserver.assertNoErrors()

        // testObserver.awaitTerminalEvent()
        testObserver.assertValue { searchList ->
            searchList.list.first().word == searchResult.word
        }
    }
}